﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kotrak.Core.Domain
{
    public class Contractor
    {
        public int ID { get; set; }

        public string Name { get; set; }

        public string Descr { get; set; }

        public string TaxCode { get; set; }

        public ContractorStatusEnum ContractorStatus { get; set; }

        public Address Address { get; set; }

        public IEnumerable <Contact> Contacts { get; set; }
    }
}
