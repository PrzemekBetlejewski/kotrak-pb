﻿using Kotrak.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kotrak.Core.Repositories
{
    public interface IRepository
    {
        Task<Contractor> GetAsync(int ID);
        Task<Contractor> GetAsync(string name);
        Task<IEnumerable<Contractor>> GetAllAsync();
        Task AddAsync(Contractor contractor);
        Task UpdateAsync(Contractor contractor);
        Task Delete(int id);
    }
}
