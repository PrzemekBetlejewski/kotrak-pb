﻿using Autofac;
using Kotrak.Infrastructure.IoC.Modules;
using Kotrak.Infrastructure.Mappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kotrak.Infrastructure.IoC
{
    public class ContainerModule : Autofac.Module
    {
        public ContainerModule()
        {

        }

        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterInstance(AutoMapperConfig.Initialize()).SingleInstance();
            builder.RegisterModule<Repository>();
            builder.RegisterModule<Service>();

        }

    }
}
