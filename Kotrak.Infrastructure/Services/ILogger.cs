﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kotrak.Infrastructure.Services
{
    public interface ILogger : IService
    {
        void ToLog(string message);
        void ToLog(string message, Exception ex);
    }
}
