﻿using Kotrak.Infrastructure.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kotrak.Infrastructure.Services
{
    public interface IContractorService : IService
    {
        Task<ContractorDTO> GetAsync(string name);
        Task<IEnumerable<ContractorDTO>> GetAllAsync();
        Task AddAsync(ContractorDTO contractorDTO);
        Task UpdateAsync(ContractorDTO contractorDTO);
        Task DeleteAsync(ContractorDTO contractorDTO);
    }
}
