﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kotrak.Infrastructure.DTO;
using Kotrak.Core.Repositories;
using AutoMapper;
using Kotrak.Core.Domain;

namespace Kotrak.Infrastructure.Services
{
    public class ContractorService : IContractorService
    {
        private IContractorRepository _contractorRepository;
        private IMapper _mapper;

        public ContractorService(IContractorRepository contractorRepository, IMapper mapper)
        {
            _contractorRepository = contractorRepository;
            _mapper = mapper;
        }
        public async Task AddAsync(ContractorDTO contractorDTO)
        {
            await _contractorRepository.AddAsync(_mapper.Map<ContractorDTO, Contractor>(contractorDTO));
        }

        public async Task DeleteAsync(ContractorDTO contractorDTO)
        {
            await _contractorRepository.Delete(contractorDTO.ID);
        }

        public async Task<IEnumerable<ContractorDTO>> GetAllAsync()
        {
            var contractors = await _contractorRepository.GetAllAsync();
            return _mapper.Map<IEnumerable<Contractor>, IEnumerable<ContractorDTO>>(contractors);
        }

        public async Task<ContractorDTO> GetAsync(string name)
        {
            var contractor = await _contractorRepository.GetAsync(name);
            return _mapper.Map<Contractor, ContractorDTO>(contractor);
        }

        public async Task UpdateAsync(ContractorDTO contractorDTO)
        {
            await _contractorRepository.UpdateAsync(_mapper.Map<ContractorDTO, Contractor>(contractorDTO));
        }
    }
}
