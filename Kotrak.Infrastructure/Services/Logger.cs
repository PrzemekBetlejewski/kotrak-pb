﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kotrak.Infrastructure.Services
{
    public class Logger : ILogger
    {
        public void ToLog(string message)
        {
            Debug.Print(String.Format("Log: {0}", message));
        }

        public void ToLog(string message, Exception ex)
        {
            Debug.Print(String.Format("Log: {0}, execption message: {1}", message, ex.Message));
        }
    }
}
