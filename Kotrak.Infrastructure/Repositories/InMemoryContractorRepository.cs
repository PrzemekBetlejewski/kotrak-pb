﻿using Kotrak.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kotrak.Core.Domain;

namespace Kotrak.Infrastructure.Repositories
{
    public class InMemoryContractorRepository : IContractorRepository
    {
        private static readonly ISet<Contractor> _contractors = new HashSet<Contractor>();

        public InMemoryContractorRepository()
        {
            _contractors.Add(new Contractor()
            {
                ID = 1,
                Name = "Audi",
                Descr = "Firma Audi",
                ContractorStatus = ContractorStatusEnum.Active,
                TaxCode = "0123456789",
                Address = new Address()
                {
                    City = "Warszawa",
                    Street = "Wielka",
                    Country = "Polska",
                    ZipCode = "00-000"
                },
                Contacts = new List<Contact>()
                 {
                     new Contact() { FirstName = "Jan", LastName = "Kowalski", Email = "Jan@kowalski.pl", Phone = "12-00000", Descr = "Kontakt do Jana Kowalskiego" }
                 }

            });

            _contractors.Add(new Contractor()
            {
                ID = 2,
                Name = "BMW",
                Descr = "Firma BMW",
                ContractorStatus = ContractorStatusEnum.Active,
                TaxCode = "0123456765",
                Address = new Address()
                {
                    City = "Warszawa",
                    Street = "Mała",
                    Country = "Polska",
                    ZipCode = "12-000"
                },
                Contacts = new List<Contact>()
                 {
                     new Contact() { FirstName = "Jan", LastName = "Kowalski", Email = "Jan@kowalski.pl", Phone = "12-00000", Descr = "Kontakt do Jana Kowalskiego" }
                 }

            });
        }
        public async Task AddAsync(Contractor contractor)
        {
            _contractors.Add(contractor);
            await Task.CompletedTask;
        }

        public async Task Delete(int id)
        {
            var contractor = await GetAsync(id);
            _contractors.Remove(contractor);
            await Task.CompletedTask;
        }

        public async Task<IEnumerable<Contractor>> GetAllAsync()
        {
            return await Task.FromResult(_contractors);
        }

        public async Task<Contractor> GetAsync(string name)
        {
            return await Task.FromResult(_contractors.SingleOrDefault(x => x.Name.ToLowerInvariant() == name.ToLowerInvariant()));
        }

        public async Task<Contractor> GetAsync(int ID)
        {
            return await Task.FromResult(_contractors.SingleOrDefault(x => x.ID == ID));
        }

        public async Task UpdateAsync(Contractor contractor)
        {
            await Task.CompletedTask;
        }
    }
}
