﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kotrak.Infrastructure.DTO
{
    public class ContractorDTO
    {
        public int ID { get; set; }

        public string Name { get; set; }

        public string Descr { get; set; }

        public string TaxCode { get; set; }

        public AddressDTO Address { get; set; }

        public IEnumerable<ContactDTO> Contacts { get; set; }
    }
}
