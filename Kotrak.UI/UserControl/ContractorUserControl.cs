﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Grid;

namespace Kotrak.UI.UserControl
{
    public partial class ContractorUserControl : EditFormUserControl
    {
        public ContractorUserControl()
        {
            InitializeComponent();
            this.SetBoundFieldName(teName, "Name");
            this.SetBoundFieldName(teDescr, "Descr");
            this.SetBoundFieldName(teTaxCode, "TaxCode");
            this.SetBoundFieldName(teStreet, "Address.Street");
            this.SetBoundFieldName(teCity, "Address.City");
            this.SetBoundFieldName(teZipCode, "Address.ZipCode");
            this.SetBoundFieldName(teCountry, "Address.Country");
        }
    }
}
