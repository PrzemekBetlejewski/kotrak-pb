﻿namespace Kotrak.UI
{
    partial class FormContractor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormContractor));
            this.gvContact = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gcContractor = new DevExpress.XtraGrid.GridControl();
            this.gvContractor = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumnName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnTaxCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnDescr = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnAddress = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnStreet = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnZipCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnCountry = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ribbon = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.bbiRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.bbiAdd = new DevExpress.XtraBars.BarButtonItem();
            this.bbiDelete = new DevExpress.XtraBars.BarButtonItem();
            this.bbiEdit = new DevExpress.XtraBars.BarButtonItem();
            this.rpMain = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.rpgMain = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonStatusBar = new DevExpress.XtraBars.Ribbon.RibbonStatusBar();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.gridColumnID = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.gvContact)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcContractor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvContractor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            this.SuspendLayout();
            // 
            // gvContact
            // 
            this.gvContact.GridControl = this.gcContractor;
            this.gvContact.Name = "gvContact";
            // 
            // gcContractor
            // 
            gridLevelNode1.LevelTemplate = this.gvContact;
            gridLevelNode1.RelationName = "gvContact";
            this.gcContractor.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gcContractor.Location = new System.Drawing.Point(12, 12);
            this.gcContractor.MainView = this.gvContractor;
            this.gcContractor.MenuManager = this.ribbon;
            this.gcContractor.Name = "gcContractor";
            this.gcContractor.Size = new System.Drawing.Size(1050, 227);
            this.gcContractor.TabIndex = 4;
            this.gcContractor.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvContractor,
            this.gvContact});
            this.gcContractor.Load += new System.EventHandler(this.gcContractor_Load);
            // 
            // gvContractor
            // 
            this.gvContractor.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnID,
            this.gridColumnName,
            this.gridColumnTaxCode,
            this.gridColumnDescr,
            this.gridColumnAddress,
            this.gridColumnStreet,
            this.gridColumnZipCode,
            this.gridColumnCountry});
            this.gvContractor.GridControl = this.gcContractor;
            this.gvContractor.Name = "gvContractor";
            this.gvContractor.OptionsBehavior.EditingMode = DevExpress.XtraGrid.Views.Grid.GridEditingMode.EditForm;
            this.gvContractor.InvalidRowException += new DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventHandler(this.gvContractor_InvalidRowException);
            this.gvContractor.ValidateRow += new DevExpress.XtraGrid.Views.Base.ValidateRowEventHandler(this.gvContractor_ValidateRow);
            // 
            // gridColumnName
            // 
            this.gridColumnName.Caption = "Nazwa";
            this.gridColumnName.FieldName = "Name";
            this.gridColumnName.Name = "gridColumnName";
            this.gridColumnName.Visible = true;
            this.gridColumnName.VisibleIndex = 0;
            // 
            // gridColumnTaxCode
            // 
            this.gridColumnTaxCode.Caption = "NIP";
            this.gridColumnTaxCode.FieldName = "TaxCode";
            this.gridColumnTaxCode.Name = "gridColumnTaxCode";
            this.gridColumnTaxCode.Visible = true;
            this.gridColumnTaxCode.VisibleIndex = 1;
            // 
            // gridColumnDescr
            // 
            this.gridColumnDescr.Caption = "Opis";
            this.gridColumnDescr.FieldName = "Descr";
            this.gridColumnDescr.Name = "gridColumnDescr";
            this.gridColumnDescr.Visible = true;
            this.gridColumnDescr.VisibleIndex = 2;
            // 
            // gridColumnAddress
            // 
            this.gridColumnAddress.Caption = "Miasto";
            this.gridColumnAddress.FieldName = "Address.City";
            this.gridColumnAddress.Name = "gridColumnAddress";
            this.gridColumnAddress.Visible = true;
            this.gridColumnAddress.VisibleIndex = 3;
            // 
            // gridColumnStreet
            // 
            this.gridColumnStreet.Caption = "Ulica";
            this.gridColumnStreet.FieldName = "Address.Street";
            this.gridColumnStreet.Name = "gridColumnStreet";
            this.gridColumnStreet.Visible = true;
            this.gridColumnStreet.VisibleIndex = 4;
            // 
            // gridColumnZipCode
            // 
            this.gridColumnZipCode.Caption = "Kod pocztowy";
            this.gridColumnZipCode.FieldName = "Address.ZipCode";
            this.gridColumnZipCode.Name = "gridColumnZipCode";
            this.gridColumnZipCode.Visible = true;
            this.gridColumnZipCode.VisibleIndex = 5;
            // 
            // gridColumnCountry
            // 
            this.gridColumnCountry.Caption = "Kraj";
            this.gridColumnCountry.FieldName = "Address.Country";
            this.gridColumnCountry.Name = "gridColumnCountry";
            this.gridColumnCountry.Visible = true;
            this.gridColumnCountry.VisibleIndex = 6;
            // 
            // ribbon
            // 
            this.ribbon.ExpandCollapseItem.Id = 0;
            this.ribbon.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbon.ExpandCollapseItem,
            this.bbiRefresh,
            this.bbiAdd,
            this.bbiDelete,
            this.bbiEdit});
            this.ribbon.Location = new System.Drawing.Point(0, 0);
            this.ribbon.MaxItemId = 5;
            this.ribbon.Name = "ribbon";
            this.ribbon.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.rpMain});
            this.ribbon.Size = new System.Drawing.Size(1074, 141);
            this.ribbon.StatusBar = this.ribbonStatusBar;
            // 
            // bbiRefresh
            // 
            this.bbiRefresh.Caption = "Odśwież";
            this.bbiRefresh.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiRefresh.Glyph")));
            this.bbiRefresh.Id = 1;
            this.bbiRefresh.Name = "bbiRefresh";
            this.bbiRefresh.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.bbiRefresh.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRefresh_ItemClick);
            // 
            // bbiAdd
            // 
            this.bbiAdd.Caption = "Dodaj";
            this.bbiAdd.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiAdd.Glyph")));
            this.bbiAdd.Id = 2;
            this.bbiAdd.Name = "bbiAdd";
            this.bbiAdd.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.bbiAdd.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiAdd_ItemClick);
            // 
            // bbiDelete
            // 
            this.bbiDelete.Caption = "Usuń";
            this.bbiDelete.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiDelete.Glyph")));
            this.bbiDelete.Id = 3;
            this.bbiDelete.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiDelete.LargeGlyph")));
            this.bbiDelete.Name = "bbiDelete";
            this.bbiDelete.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.bbiDelete.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiDelete_ItemClick);
            // 
            // bbiEdit
            // 
            this.bbiEdit.Caption = "Edytuj";
            this.bbiEdit.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiEdit.Glyph")));
            this.bbiEdit.Id = 4;
            this.bbiEdit.Name = "bbiEdit";
            this.bbiEdit.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.bbiEdit.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiEdit_ItemClick);
            // 
            // rpMain
            // 
            this.rpMain.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.rpgMain});
            this.rpMain.Name = "rpMain";
            this.rpMain.Text = "Narzędzia główne";
            // 
            // rpgMain
            // 
            this.rpgMain.ItemLinks.Add(this.bbiRefresh, true);
            this.rpgMain.ItemLinks.Add(this.bbiAdd, true);
            this.rpgMain.ItemLinks.Add(this.bbiDelete);
            this.rpgMain.ItemLinks.Add(this.bbiEdit);
            this.rpgMain.Name = "rpgMain";
            this.rpgMain.Text = "Dane";
            // 
            // ribbonStatusBar
            // 
            this.ribbonStatusBar.Location = new System.Drawing.Point(0, 392);
            this.ribbonStatusBar.Name = "ribbonStatusBar";
            this.ribbonStatusBar.Ribbon = this.ribbon;
            this.ribbonStatusBar.Size = new System.Drawing.Size(1074, 27);
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.gcContractor);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 141);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(1074, 251);
            this.layoutControl1.TabIndex = 2;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(1074, 251);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.gcContractor;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(1054, 231);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // gridColumnID
            // 
            this.gridColumnID.Caption = "ID";
            this.gridColumnID.FieldName = "ID";
            this.gridColumnID.Name = "gridColumnID";
            this.gridColumnID.Visible = true;
            this.gridColumnID.VisibleIndex = 7;
            // 
            // FormContractor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1074, 419);
            this.Controls.Add(this.layoutControl1);
            this.Controls.Add(this.ribbonStatusBar);
            this.Controls.Add(this.ribbon);
            this.Name = "FormContractor";
            this.ShowIcon = false;
            this.Text = "Lista kontrahentów";
            ((System.ComponentModel.ISupportInitialize)(this.gvContact)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcContractor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvContractor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.Ribbon.RibbonControl ribbon;
        private DevExpress.XtraBars.Ribbon.RibbonPage rpMain;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgMain;
        private DevExpress.XtraBars.Ribbon.RibbonStatusBar ribbonStatusBar;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraGrid.GridControl gcContractor;
        private DevExpress.XtraGrid.Views.Grid.GridView gvContractor;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraBars.BarButtonItem bbiRefresh;
        private DevExpress.XtraBars.BarButtonItem bbiAdd;
        private DevExpress.XtraBars.BarButtonItem bbiDelete;
        private DevExpress.XtraBars.BarButtonItem bbiEdit;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnName;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnTaxCode;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnDescr;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnAddress;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnStreet;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnZipCode;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnCountry;
        private DevExpress.XtraGrid.Views.Grid.GridView gvContact;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnID;
    }
}