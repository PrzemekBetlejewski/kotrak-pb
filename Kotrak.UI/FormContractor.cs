﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraBars;
using Kotrak.Infrastructure.Services;
using Kotrak.UI.UserControl;
using DevExpress.XtraGrid.Views.Grid;
using Kotrak.Infrastructure.DTO;
using DevExpress.XtraEditors;

namespace Kotrak.UI
{
    public partial class FormContractor : DevExpress.XtraEditors.XtraForm
    {
        private IContractorService _contractorService;
        private ILogger _logger;
        private FormEditMode _formEditMode;

        public FormContractor(IContractorService contractorService, ILogger logger)
        {
            _contractorService = contractorService;
            _logger = logger;
            InitializeComponent();
            ContractorUserControl contractorUserControl = new ContractorUserControl();
            gvContractor.OptionsEditForm.CustomEditFormLayout = contractorUserControl;

        }

        private async void gcContractor_Load(object sender, EventArgs e)
        {
            gcContractor.DataSource = await _contractorService.GetAllAsync();
        }

        private void bbiAdd_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                _formEditMode = FormEditMode.Add;
              
            }

            catch(Exception ex)
            {

            }
        }

        enum FormEditMode
        {
            None = 0,
            Add = 1,
            Edit = 2
        }

        private void bbiEdit_ItemClick(object sender, ItemClickEventArgs e)
        {
            _formEditMode = FormEditMode.Edit;
            gvContractor.ShowEditForm();
        }

        private async void gvContractor_ValidateRow(object sender, DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs e)
        {
            GridView view = sender as GridView;
            ContractorUserControl contractorUserControl = (ContractorUserControl)view.OptionsEditForm.CustomEditFormLayout;

            try
            {
                //Przykład walidacji formularza
                string name = Convert.ToString(view.GetRowCellValue(e.RowHandle, view.Columns["Name"]));
                if (String.IsNullOrEmpty(name))
                {
                    e.Valid = false;
                    view.SetColumnError(view.Columns["Name"], "Pole jest wymagane");
                    return;
                }
                
                if (_formEditMode == FormEditMode.Edit)
                {
                    ContractorDTO contractorDTO = gvContractor.GetRow(e.RowHandle) as ContractorDTO;
                    await _contractorService.UpdateAsync(contractorDTO);
                }

            }

            catch (Exception ex)
            {
                _logger.ToLog("Error - edit contractor", ex);
            }
        }

        private void gvContractor_InvalidRowException(object sender, DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventArgs e)
        {
            e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.NoAction;
        }

        private async void bbiDelete_ItemClick(object sender, ItemClickEventArgs e)
        {
            if(XtraMessageBox.Show(this, "Czy napewno chcesz usunąc danego kontrahenta?", "Informacja", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                int focusedRow = gvContractor.FocusedRowHandle;
                var focusedRowContractorId = Convert.ToInt32(gvContractor.GetRowCellValue(focusedRow, gvContractor.Columns["ID"]));
                
                //Brakło czasu
                //ContractorDTO contractorDTO = gvContractor.GetRow(focusedRowContractorId) as ContractorDTO;
                //await _contractorService.DeleteAsync(contractorDTO);
            }

            else
            {

            }
        }

        private void bbiRefresh_ItemClick(object sender, ItemClickEventArgs e)
        {
            gvContractor.RefreshData();
        }
    }
}