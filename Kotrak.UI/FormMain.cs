﻿using Autofac;
using DevExpress.LookAndFeel;
using DevExpress.Xpo.Logger;
using DevExpress.XtraSplashScreen;
using Kotrak.Infrastructure.IoC;
using Kotrak.Infrastructure.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Kotrak.UI
{
    public partial class FormMain : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        private Autofac.IContainer _container;
        

        public FormMain()
        {
            SplashScreenManager.ShowForm(typeof(SplashScreenFormMain));
            SetAutoFac();
            InitializeComponent();
            UserLookAndFeel.Default.StyleChanged += Default_StyleChanged;
            SplashScreenManager.CloseForm();
            SetStartForm();
        }

        private void SetAutoFac()
        {
            var builder = new ContainerBuilder();
            builder.RegisterModule(new ContainerModule());
            _container = builder.Build();
        }

        private void SetStartForm()
        {
            foreach (Form form in this.MdiChildren)
            {
                if (form is FormStart)
                    return;
                else
                    form.Close();
            }
            FormStart child = new FormStart();
            child.WindowState = FormWindowState.Maximized;
            child.MdiParent = this;
            child.Show();
        }

        private void Default_StyleChanged(object sender, EventArgs e)
        {
            global::Kotrak.UI.Properties.Settings.Default.activeSkin = UserLookAndFeel.Default.ActiveSkinName;
            global::Kotrak.UI.Properties.Settings.Default.Save();
        }

        private void navBarItemContractor_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            SplashScreenManager.ShowWaitForm();
            SetContractorForm();
            SplashScreenManager.CloseWaitForm();
        }

        private void SetContractorForm()
        {
            foreach (Form form in this.MdiChildren)
            {
                if (form is FormContractor)
                    return;
                else
                    form.Close();
            }
            using (var scope = _container.BeginLifetimeScope())
            {
                var contractor = scope.Resolve<IContractorService>();
                var logger = scope.Resolve<Infrastructure.Services.ILogger>();

                FormContractor child = new FormContractor(contractor, logger);
                child.WindowState = FormWindowState.Maximized;
                child.MdiParent = this;
                child.Show();
            }
        }

        private void bbiClose_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.Close();
        }
    }
}
